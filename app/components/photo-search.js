import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';

export default Ember.Component.extend({
  flickr: Ember.inject.service(),
  location: Ember.inject.service(),
  photos: null,
  searchTerms: null,

  init() {
    this._super(...arguments);

    this.get('location').watchLocation(() => {
      let searchTerms = this.get('searchTerms');

      if (searchTerms) {
        let event = { target: { value: searchTerms } };
        this.get('search').perform(event);
      }
    });
  },

  search: task(function *(event) {
    let terms = event.target.value;
    this.set('searchTerms', terms);
    yield timeout(500);
    let coords = this.get('location.currentLocation');
    let results = yield this.get('flickr').search(terms, coords);
    this.set('photos', results);
  }).restartable(),

  willDestroy() {
    this._super(...arguments);

    this.get('location').stopWatchingLocation();
  }
});
