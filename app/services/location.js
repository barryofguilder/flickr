import Ember from 'ember';

export default Ember.Service.extend(Ember.Evented, {
  currentLocation: null,
  _locationWatchId: null,

  watchLocation(callback) {
    let watchId = navigator.geolocation.watchPosition((position) => {
      this.set('currentLocation', position.coords);
      callback(position.coords);
    });
    this.set('_locationWatchId', watchId);
  },

  stopWatchingLocation() {
    let watchId = this.get('_locationWatchId');

    if (watchId) {
      navigator.geolocation.clearWatch(watchId);
    }
  }
});
