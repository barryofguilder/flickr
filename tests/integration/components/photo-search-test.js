import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import wait from 'ember-test-helpers/wait';
import hbs from 'htmlbars-inline-precompile';

const fakeFlickr = Ember.Service.extend({
  search() {
    return Ember.RSVP.reject();
  }
});

const fakeLocation = Ember.Service.extend({
  watchLocation() {},
  stopWatchingLocation() {}
});

moduleForComponent('photo-search', 'Integration | Component | photo search', {
  integration: true,
  beforeEach() {
    this.register('service:flickr', fakeFlickr);
    this.inject.service('flickr', { as: 'flickrStub' });

    this.register('service:location', fakeLocation);
    this.inject.service('location', { as: 'locationStub' });
  }
});

test('it yields nothing when have not searched', function(assert) {
  assert.expect(1);
  let done = assert.async();

  this.get('flickrStub').search = () => {
    assert.ok(false, 'Search should not be called');
    return Ember.RSVP.resolve([1,2,3]);
  };

  this.render(hbs`
    {{#photo-search as |searchResults|}}
      {{#each searchResults as |result|}}
        <div class="test-result"></div>
      {{/each}}
    {{/photo-search}}
  `);

  wait().then(() => {
    assert.equal(this.$('.test-result').length, 0, 'No results show');
    done();
  });
});

test('it searches as you type', function(assert) {
  assert.expect(2);

  let done = assert.async();

  let searchCount = 0;
  this.get('flickrStub').search = () => {
    searchCount++;
    return Ember.RSVP.resolve([1,2,3]);
  };

  this.render(hbs`
    {{#photo-search as |searchResults|}}
      {{#each searchResults as |result|}}
        <div class="test-result"></div>
      {{/each}}
    {{/photo-search}}
  `);

  this.$('.test-search-input').val('n').trigger('input');
  this.$('.test-search-input').val('ny').trigger('input');
  this.$('.test-search-input').val('nyc').trigger('input');
  wait().then(() => {
    assert.equal(searchCount, 1, 'Search should only be hit once');
    assert.equal(this.$('.test-result').length, 3, 'Three results show');
    done();
  });
});

test('it triggers a new search when the location changes', function(assert) {
  assert.expect(2);

  let searchCount = 0;
  let initialSearchDone = assert.async();
  let searchAfterLocationChangeDone = assert.async();

  this.get('flickrStub').search = function() {
    searchCount++;
    return Ember.RSVP.resolve([]);
  };

  this.get('locationStub').currentLocation = {
    latitude: 40.6437829,
    longitude: -74.2581883
  };

  this.render(hbs`{{photo-search}}`);

  this.$('.test-search-input').val('empire').trigger('input');

  wait().then(() => {
    assert.equal(searchCount, 1, 'Search has happened once');
    initialSearchDone();
  });

  wait().then(() => {
    this.get('locationStub').currentLocation = {
      latitude: 40.7053111,
      longitude: -74.2581883
    };

    // TODO: Figure out how the heck I can detect the change
  });

  wait().then(() => {
    assert.equal(searchCount, 2, 'Search has happened twice');
    searchAfterLocationChangeDone();
  });

});
